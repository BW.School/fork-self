import java.awt.*;
import java.util.ArrayList;

public class Steganography {
    public static void clearLow(Pixel P) {
        P.setBlue(P.getBlue() & 0b1111_1100);
        P.setGreen(P.getGreen() & 0b1111_1100);
        P.setRed(P.getRed() & 0b1111_1100);
    }

    public static Picture testClearLow(Picture Pic) {
        Picture newPic = new Picture(Pic);
        for (int x = 0; x < Pic.getWidth(); x++) {
            for (int y = 0; y < Pic.getHeight(); y++) {
                Pixel currPix = newPic.getPixel(x, y);
                currPix.setBlue(currPix.getBlue() & 0b1111_1100);
                currPix.setGreen(currPix.getGreen() & 0b1111_1100);
                currPix.setRed(currPix.getRed() & 0b1111_1100);
            }
        }
        return newPic;
    }

    public static void SetLow(Pixel p, Color c) {
        p.setBlue((c.getBlue() >> 6) & p.getBlue());
        p.setRed((c.getRed() >> 6) & p.getRed());
        p.setGreen((c.getGreen() >> 6) & p.getGreen());
    }

    public static Picture testSetLow(Picture p, Color c) {
        for (int x = 0; x < p.getWidth(); x++) {
            for (int y = 0; y < p.getHeight(); y++) {
                Pixel currPix = p.getPixel(x, y);
                Steganography.SetLow(currPix, c);
            }
        }
        return p;
    }

    public static Picture revealPicture(Picture hidden) { //THANKS WILL SAVED ME
        Picture copy = new Picture(hidden);
        Pixel[][] pixels = copy.getPixels2D();
        Pixel[][] source = hidden.getPixels2D();
        for (int r = 0; r < pixels.length; r++) {
            for (int c = 0; c < pixels[0].length; c++) {
                Color col = source[r][c].getColor();
                pixels[r][c].setGreen((col.getGreen() << 6) & 0xFF);
                pixels[r][c].setBlue((col.getBlue() << 6) & 0xFF);
                pixels[r][c].setRed((col.getRed() << 6) & 0xFF);
            }
        }
        return copy;
    }


    public static boolean canHide(Picture source, Picture secret) {
        return (source.getHeight() >= secret.getHeight()) && (source.getWidth() >= secret.getWidth());
    }

    static Picture hidePicture(Picture source, Picture secret, int horizontal, int vertical) {
        if (!canHide(source, secret)) return null;
        Picture newPic = new Picture(source);
        Pixel[][] source2D = newPic.getPixels2D();
        Pixel[][] secret2D = secret.getPixels2D();
        for (int x = 0; x < secret.getWidth(); x++) {
            for (int y = 0; y < secret.getHeight(); y++) {
                source2D[y + vertical][x + horizontal].setRed((source2D[y + vertical][x + horizontal].getRed() & -4) | (secret2D[y][x].getRed() >> 6));
                source2D[y + vertical][x + horizontal].setGreen((source2D[y + vertical][x + horizontal].getGreen() & -4) | (secret2D[y][x].getGreen() >> 6));
                source2D[y + vertical][x + horizontal].setBlue((source2D[y + vertical][x + horizontal].getBlue() & -4) | (secret2D[y][x].getBlue() >> 6));
            }
        }
        return newPic;
    }

    public static boolean isSame(Picture one, Picture two) {
        if (!canHide(one, two)) return false;
        if (!canHide(two, one)) return false;
        boolean passes = true;
        Pixel[][] one2D = one.getPixels2D();
        Pixel[][] two2D = two.getPixels2D();
        for (int x = 0; x < one.getWidth(); x++) {
            for (int y = 0; y < one.getHeight(); y++) {
                if (!one2D[y][x].getColor().equals(two2D[y][x].getColor())) {
                    passes = false;
                }
            }
        }
        return passes;
    }

    public static ArrayList<Point> findDifferences(Picture one, Picture two) {
        ArrayList<Point> differencePoints = new ArrayList<Point>();
        if (!canHide(one, two)) return differencePoints;
        if (!canHide(two, one)) return differencePoints;
        Pixel[][] one2D = one.getPixels2D();
        Pixel[][] two2D = two.getPixels2D();
        for (int x = 0; x < one.getWidth(); x++) {
            for (int y = 0; y < one.getHeight(); y++) {
                if (!one2D[y][x].getColor().equals(two2D[y][x].getColor())) {
//                    System.out.println(y + ", " + x);
                    differencePoints.add(new Point(x, y));
                }
            }
        }
        return differencePoints;
    }

    public static Picture showDifferentArea(Picture pic, ArrayList<Point> points) {
        Picture result = new Picture(pic);
        int minR = pic.getHeight() - 1;
        int minC = pic.getWidth() - 1;
        int maxR = 0;
        int maxC = 0;
        for (Point p : points) {
            int r = (int) p.getY();
            int c = (int) p.getX();
            if (r < minR) {minR=r;}
            if (r > maxR) {maxR=r;}
            if (c < minC) {minC=c;}
            if (c > maxC) {maxC=c;}
        }
        for (int x = minC; x < maxC; x++) {
            result.getPixel(x, minR).setColor(Color.red);
            result.getPixel(x, maxR).setColor(Color.red);
        }
        for (int y = minR + 1; y < maxR; y++) {
            result.getPixel(minC, y).setColor(Color.red);
            result.getPixel(maxC, y).setColor(Color.red);
        }
        return result;
    }

    public static ArrayList<Integer> encodeString(String s) {
        s = s.toUpperCase();
        String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        ArrayList<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) == ' ') {
                result.add(27);
            }
            else {
                result.add(alpha.indexOf(s.substring(i, i+1))+1);
            }
        }
        result.add(0);
        return result;
    }

    public static String decodeString(ArrayList<Integer> codes)
    {
        StringBuilder result= new StringBuilder();
        String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i=0; i < codes.size(); i++)
        {
            if (codes.get(i) == 27)
            {
                result.append(" ");
            }
            else
            {
                result.append(alpha.charAt(codes.get(i) - 1));
            }
        }
        return result.toString();
    }


    private static int[] getBitPairs(int num) {
        int[] bits = new int[3];
        int code = num;
        for (int i = 0; i < 3; i++) {
            bits[i] = code % 4;
            code = code / 4;
        }
        return bits;
    }

    public static Picture hideText(Picture source, String s) { //Picture or void?
        Picture newPic = new Picture(source);
        Pixel[][] pixels2D = newPic.getPixels2D();
        ArrayList<Integer> encode = encodeString(s);
        int currChar = 0;
        for (int r = 0; r < pixels2D.length && currChar < encode.size(); r++) {
            for (int c = 0; c < pixels2D[0].length && currChar < encode.size(); c++) {
                clearLow(pixels2D[r][c]);
                Color col = pixels2D[r][c].getColor();
                int[] bits = getBitPairs(encode.get(currChar));
                pixels2D[r][c].setColor(new Color(col.getRed()+bits[0], col.getGreen()+bits[1], col.getBlue()+bits[2]));
                currChar++;
            }
        }
        return newPic;
    }

    public static String revealText(Picture source) {
        Pixel[][] pixels2D = source.getPixels2D();
        ArrayList<Integer> codes = new ArrayList<>();
        boolean done = false;
        for (int r = 0; r < pixels2D.length && !done; r++) {
            for (int c = 0; c < pixels2D[0].length && !done; c++) {
                Pixel pixel = pixels2D[r][c];
                Color col = pixel.getColor();
                int code = col.getRed() % 4 + (col.getGreen()% 4) * 4 + (col.getBlue() % 4) * 16; //idk what the multi is for
                if (code == 0) {
                    done = true;
                } else {
                    codes.add(code);
                }
            }
        }
        return decodeString(codes);
    }


    public static void main(String[] args) {
//        Picture beach = new Picture("resources/beach.jpg");
//        beach.explore();
//        Picture copy = testClearLow(beach);
//        copy.explore();
//        Picture beach2 = new Picture("resources/beach.jpg");
//        beach2.explore();
//        Picture copy2 = testSetLow(beach2, Color.PINK); //makes black box, cannot remember og use
//        copy2.explore();
//        Picture copy3 = revealPicture(copy2);
//        copy3.explore();
        //works below
        Picture mark = hidePicture(new Picture("resources/blue-mark.jpg"), new Picture("resources/robot.jpg"), 10, 100);
        Picture copy4 = revealPicture(mark);
        copy4.explore();
        Picture Pic1 = new Picture("resources/robot.jpg");
        Picture Pic2 = new Picture("resources/beach.jpg");
        Picture Hidden = hidePicture(Pic2, Pic1, 65, 102);
        revealPicture(Hidden).explore();
        Picture arch = new Picture("resources/arch.jpg");
        Picture robot = new Picture("resources/robot.jpg");
        Picture arch2 = hidePicture(arch, robot, 65, 102);
        revealPicture(arch2).explore();
        System.out.println(findDifferences(arch, arch).size());
        System.out.println(findDifferences(arch, robot).size());
        System.out.println(findDifferences(arch, arch2).size());
        Picture hall = new Picture("resources/femaleLionAndHall.jpg");
        Picture robot2 = new Picture("resources/robot.jpg");
        Picture flower2 = new Picture("resources/flower1.jpg");
        //hide
        Picture hall2 = hidePicture(hall, robot2, 50, 300);
        Picture hall3 = hidePicture(hall2, flower2, 115, 275);
        hall3.explore();
        if (!isSame(hall, hall3)) {
            Picture hall4 = showDifferentArea(hall, findDifferences(hall, hall3));
            hall4.show();
            Picture unhiddenHall3 = revealPicture(hall3);
            unhiddenHall3.explore();
        }
        Picture wall = new Picture("resources/wall.jpg");
        Picture hiddenTxt = hideText(wall, "yabadabado");
        String revealed = revealText(hiddenTxt);
        System.out.println(revealed);
    }
}
